Tugas Hari ke-10 SQL

1. Membuat Database
Sebelum membuat database, sebelumnya kita harus masuk dulu ke maria db, dengan cara ketik :
mysql -u root

Membuat database myshop dengan command :
create database myshop;
 
2. Membuat Table di Dalam Database

Sebelum membuat tabel, masuk dulu ke database myshop dengan command : 
use myshop;

Membuat TABEL users:
create table users(id int AUTO_INCREMENT, name varchar(255), email varchar(255), password varchar(255), PRIMARY KEY (id));

Membuat TABEL categories:
create table categories(id int AUTO_INCREMENT, name varchar(255), PRIMARY KEY (id));

Membuat TABEL items:
create table items(id int AUTO_INCREMENT, name varchar(255), description varchar(255), price int, stock int, categories_id int, PRIMARY KEY (id), FOREIGN KEY (categories_id) REFERENCES categories(id));

3. Memasukkan Data pada Tabel

Tabel users:
insert into users values ('','John Doe', 'john@doe.com', 'john123'), ('','Jane Doe', 'jane@doe.com', 'jenita123');

Tabel categories:
insert into categories values ('', 'gadget'),('', 'cloth'),('','men'),('','women'),('','branded');

Tabel items:
insert into items values ('','Sumsang b50','hape keren dari merek sumsang',4000000,100,1), ('', 'Uniklooh', 'baju keren dari brand ternama',500000,50,2),('','IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);

4. Mengambil Data dari Database

a. Mengambil data users
select id, name, email from users;

b. Mengambil data items
-  select * from items where price > 1000000;
-  select * from items where name LIKE '%sang%';

c. Menampilkan data items join dengan kategori
SELECT items.name, items.description, items.price, items.stock, items.categories_id, categories.name FROM items INNER JOIN categories ON items.categories_id=categories.id;


5. Mengubah Data dari Database
UPDATE items SET price=2500000 where name='sumsang b50';
